#!/bin/sh

arch="`uname -p`"

fetch -o /tmp -q https://bitbucket.org/gugabsd/pfsense-samba3/downloads/samba-3.6.18-${arch}.pbi
pbi_add --no-checksig /tmp/samba-3.6.18-${arch}.pbi
rm -rf /tmp/samba-3.6.18-${arch}.pbi

fetch -o /usr/local/pkg -q https://bitbucket.org/gugabsd/pfsense-samba3/downloads/samba3.inc
fetch -o /usr/local/pkg -q https://bitbucket.org/gugabsd/pfsense-samba3/downloads/samba3.xml
fetch -o /usr/local/www -q https://bitbucket.org/gugabsd/pfsense-samba3/downloads/samba3/fbegin.inc
fetch -o /usr/local/www/javascript -q https://bitbucket.org/gugabsd/pfsense-samba3/downloads/jquery-1.9.1.min.js

fetch -o - -q https://bitbucket.org/gugabsd/pfsense-samba3/downloads/samba-libs-${arch}.tar.gz | tar -C / -zxpf -

echo 'samba_enable="YES"' > /etc/rc.conf.local
echo 'winbindd_enable="YES"' >> /etc/rc.conf.local

cd /usr/local/etc/rc.d/
ln -s samba samba.sh

fetch -o /tmp -q https://bitbucket.org/gugabsd/pfsense-samba3/downloads/squid_with_ntlm.patch
cd /usr/local/pkg
patch -p0 < /tmp/squid_with_ntlm.patch

mkdir -p /var/db/samba/winbindd_privileged
chown -R :proxy /var/db/samba/winbindd_privileged
chmod -R 0750 /var/db/samba/winbindd_privileged

ln -s /usr/pbi/samba-${arch}/etc/smb.conf /usr/local/etc/smb.conf
